console.log("Hello World");

 let AboutTime = {
		Name_of_the_Movie: 'About Time',
		Name_of_the_Director: 'Richard Curtis',
		Year: 2013,
		Summary: 'At the age of 21, Tim discovers he can travel in time and change what happens and has happened in his own life. His decision to make his world a better place by getting a girlfriend turns out not to be as easy as you might think.'
		}	

	console.log(AboutTime);


 let Dominion = {
		Name_of_the_Movie: 'Jurassic World Dominion',
		Name_of_the_Director: 'Colin Trevorrow',
		Year: 2022,
		Summary: 'Four years after the destruction of Isla Nublar, Biosyn operatives attempt to track down Maisie Lockwood, while Dr Ellie Sattler investigates a genetically engineered swarm of giant insects.'
		}	

	console.log(Dominion);

 let Strange = {
		Name_of_the_Movie: 'Doctor Strange in the Multiverse of Madness',
		Name_of_the_Director: 'Sam Raimi',
		Year: 2022,
		Summary: 'Doctor Strange teams up with a mysterious teenage girl from his dreams who can travel across multiverses, to battle multiple threats, including other-universe versions of himself, which threaten to wipe out millions across the multiverse. They seek help from Wanda the Scarlet Witch, Wong and others..'
		}	

	console.log(Strange);